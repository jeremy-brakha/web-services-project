package org.webservices;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnection {
    public static Connection getConnection() throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // TODO: Changer l'url de la base de données - voir si on fait un Docker
        String url = "jdbc:postgresql://localhost:5432/soapdb?charSet=UTF-8";

        String username = "postgres";
        String password = "postgres";

        return DriverManager.getConnection(url, username, password);
    }

    public static void createTables() throws SQLException {
        try (Connection connection = getConnection()) {
            Statement statement = connection.createStatement();

            //create users table
            statement.executeUpdate(
                    "CREATE TABLE users (id SERIAL PRIMARY KEY, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL);"
            );

            //create reservations table
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS reservations (id SERIAL PRIMARY KEY, " +
                    "train_id INT NOT NULL, travel_class " +
                    "VARCHAR(255) NOT NULL, flexible VARCHAR(255) NOT NULL, reservation_date TIMESTAMP NOT NULL)");
        }
    }

    public static void main(String[] args) throws SQLException {
        createTables();
    }
}

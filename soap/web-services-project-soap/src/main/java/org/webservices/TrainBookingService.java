package org.webservices;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe est le service SOAP qui va être appelé par le client.
 */
@WebService
public class TrainBookingService {

    /**
     * Cette méthode permet à un utilisateur de s'authentifier.
     * @param username  Le nom d'utilisateur de l'utilisateur.
     * @param password  Le mot de passe de l'utilisateur.
     * @return True si l'utilisateur est connecté, False sinon.
     */
    @WebMethod
    public boolean authenticateUser(String username, String password) {
        try (Connection connection = DBConnection.getConnection()) {
            // Hash the submitted password using SHA-256
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byte[] hash = md.digest();
            String hashedPassword = bytesToHex(hash);
            // Retrieve the stored hashed password from the users table
            PreparedStatement statement = connection.prepareStatement("SELECT password FROM users WHERE username = ?");
            statement.setString(1, username);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                String storedHashedPassword = result.getString("password");
                // Compare the submitted hashed password to the stored hashed password
                if (hashedPassword.equals(storedHashedPassword)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Cette méthode permet à un utilisateur de s'inscrire.
     * @param username  Le nom d'utilisateur de l'utilisateur.
     * @param password  Le mot de passe de l'utilisateur.
     * @return True si l'utilisateur est inscrit, False sinon.
     */
    @WebMethod
    public boolean registerUser(String username, String password) {
        try (Connection connection = DBConnection.getConnection()) {
            // Hash the password using SHA-256
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byte[] hash = md.digest();
            String hashedPassword = bytesToHex(hash);
            // Insert the new user into the users table
            PreparedStatement statement = connection.prepareStatement("INSERT INTO users (username, password) VALUES (?, ?)");
            statement.setString(1, username);
            statement.setString(2, hashedPassword);
            int result = statement.executeUpdate();
            if (result == 1) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    /**
     * Cette méthode permet à un utilisateur de réserver un train.
     * @param trainId  L'identifiant du train.
     * @param travelClass  La classe de voyage.
     * @param flexible  Si le voyage est flexible ou non.
     * @return True si le train est réservé, False sinon.
     */
    public boolean makeReservation(int trainId, String travelClass, Boolean flexible) {
        try (Connection connection = DBConnection.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO reservations " +
                    "(train_id, travel_class, flexible, reservation_date) VALUES (?, ?, ?, NOW())");
            statement.setInt(1, trainId);
            statement.setString(2, travelClass);
            statement.setBoolean(3, flexible);
            int result = statement.executeUpdate();

            if (result == 1) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @return Tous les trains.
     */
    @WebMethod
    public String getTrains() {
        try {
            String url = "http://localhost:5000/trains";

            CloseableHttpClient client = HttpClients.createDefault();

            HttpGet request = new HttpGet(url);

            CloseableHttpResponse response = client.execute(request);

            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param departureStation  La gare de départ.
     * @param arrivalStation  La gare d'arrivée.
     * @param outboundDate  La date de départ.
     * @param returnDate  La date de retour.
     * @param numberOfTickets Le nombre de billets.
     * @param travelClass La classe de voyage.
     * @Return La liste des trains correspondant.
     */
    @WebMethod
    public String searchTrains(String departureStation, String arrivalStation, String outboundDate, String returnDate, int numberOfTickets, String travelClass) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/search_trains/" + departureStation + "/" + arrivalStation + "/" + outboundDate + "/" + returnDate + "/" + numberOfTickets + "/" + travelClass;
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête HTTP
            HttpGet request = new HttpGet(url);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @WebMethod
    public String search_trains_departure_station_arrival_station_outbound_date_arrival_date (String departure_station, String arrival_station, String outbound_date, String arrival_date) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/search_trains/departure_arrival_outbound_return/" + departure_station + "/" + arrival_station + "/" + outbound_date + "/" + arrival_date;
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête HTTP
            HttpGet request = new HttpGet(url);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     *
     * @param arrival_station La gare d'arrivée.
     * @return la liste de trains correspondant.
     */
    @WebMethod
    public String  search_trains_arrival_station ( String arrival_station ) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/search_trains/arrival_station/" + arrival_station;
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête HTTP
            HttpGet request = new HttpGet(url);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param outbound_date La date de départ.
     * @return la liste de trains correspondant.
     */
    @WebMethod
    public String search_trains_outbound_date ( String outbound_date ) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/search_trains/outbound_date/" + outbound_date;
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête HTTP
            HttpGet request = new HttpGet(url);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param travel_class La classe de voyage.
     * @return la liste de trains correspondant.
     */
    @WebMethod
    public String search_trains_travel_class ( String travel_class ) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/search_trains/travel_class/" + travel_class;
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête HTTP
            HttpGet request = new HttpGet(url);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param travel_class La classe de voyage.
     * @param number_of_tickets Le nombre de billets.
     * @return la liste de trains correspondant.
     */
    @WebMethod
    public String search_trains_travel_class_number_of_tickets ( String travel_class , int number_of_tickets ) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/search_trains/tickets_number/" + travel_class + "/" + number_of_tickets;
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête HTTP
            HttpGet request = new HttpGet(url);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param number_of_tickets Le nombre de billets.
     * @return la liste de trains correspondant.
     */
    @WebMethod
    public String search_trains_number_of_tickets ( int number_of_tickets ) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/search_trains/tickets_number/" + number_of_tickets;
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête HTTP
            HttpGet request = new HttpGet(url);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param departure_station La gare de départ.
     * @param arrival_station La gare d'arrivée.
     * @return la liste de trains correspondant.
     */
    @WebMethod
    public String search_trains_departure_station_arrival_station ( String departure_station , String arrival_station ) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/search_trains/" + departure_station + "/" + arrival_station;
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête HTTP
            HttpGet request = new HttpGet(url);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param departure_station La gare de départ.
     * @param arrival_station La gare d'arrivée.
     * @param outbound_date  La date de départ.
     * @return la liste de trains correspondant.
     */
    @WebMethod
    public String search_trains_departure_arrival_station_outbound_date ( String departure_station , String arrival_station , String outbound_date ) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/search_trains/" + departure_station + "/" + arrival_station + "/" + outbound_date;
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête HTTP
            HttpGet request = new HttpGet(url);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Méthode pour récupérer la liste des trains
     * @param departure_station gare de départ du train
     * @param arrival_station gare d'arrivée du train
     * @param outbound_date date de départ du train
     * @param return_date date de retour du train
     * @return la liste des trains
     */
    @WebMethod
    public String search_trains_departure_arrival_station_outbound_date_return_date ( String departure_station , String arrival_station , String outbound_date , String return_date ) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/search_trains/" + departure_station + "/" + arrival_station + "/" + outbound_date + "/" + return_date;
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête HTTP
            HttpGet request = new HttpGet(url);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            String jsonResponse = EntityUtils.toString(response.getEntity());
            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Cette méthode va appeler le service REST pour réserver un train.
     * @param trainId l'identifiant du train à réserver.
     * @param travelClass la classe de voyage.
     * @param flexible la flexibilité.
     * @Return Le train réservé.
     */
    @WebMethod
    public String bookTrain(int trainId, String travelClass, boolean flexible) {
        try {
            // création de l'url pour appeler le service REST
            String url = "http://localhost:5000/trains/book";
            // création du client HTTP
            CloseableHttpClient client = HttpClients.createDefault();
            // création de la requête POST
            HttpPost request = new HttpPost(url);
            request.setHeader("Content-Type", "application/x-www-form-urlencoded");

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("train_id", Integer.toString(trainId)));
            params.add(new BasicNameValuePair("travel_class", travelClass));
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
            request.setEntity(entity);
            // envoie de la requête et récupération de la réponse
            CloseableHttpResponse response = client.execute(request);
            // conversion de la réponse en train
            String jsonResponse = EntityUtils.toString(response.getEntity());

            System.out.println(jsonResponse);

            try {

                int userId = 1;
                Boolean reservationSucceeded = makeReservation(trainId, travelClass, flexible);
                if (reservationSucceeded) {
                    return "Réservation effectuée avec succès";
                } else {
                    return "Réservation sur le train " + trainId + " impossible";
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return "Réservation sur le train " + trainId + " impossible";
            }
//            return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}

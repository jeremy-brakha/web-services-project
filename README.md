# Projet Web Services

## Membres du projet

* Dylan EDWARD
* Jérémy BRAKHA

## Nécessaires (Pour Windows) Sorry pour Docker, on aura essayé

Bien se placer dans la branche *main*

## rest

* Se placer dans le dossier rest
* pip install -r requirements.txt
* $env:FLASK_APP = "flaskr" sur Windows
* flask init-db (Pour initialiser la bdd)
* flask run

## Postgresql

1. Pour que le fichier *DBConnection.java* fonctionne (qui se trouve dans la partie soap), il faut :
2. Créer ou avoir un utilisateur avec comme username *postgres* et un password *postgres*
3. Lui attribuer une base de données du nom de *soapdb*

## soap

* Installer Glassfish5 ([Glassfish](https://javaee.github.io/glassfish/download)) Bien installer "Full Platform".
* Bien avoir installer Maven sur sa machine ([Maven](https://maven.apache.org/install.html)) et dans son path
* Bien avoir installer JDK 8 pour faire fonctionner le projet ([JDK 8](https://www.oracle.com/fr/java/technologies/javase/javase8-archive-downloads.html))
* Se placer dans soap/web-service-project-soap et run *mvn install* qui génère un .war dans le dossier target
* copier le fichier .war dans le répertoire glassfish5/glassfish/domains/domain1/autodeploy
* se placer dans dans le répertoire glassfish5/bin et run *asadmin start-domain domain1*
* si une erreur comme **s**celle-ci se produit "org.apache.jasper.JasperException: PWC6345: There is an error in invoking javac. A full JDK (not just JRE) is required" il faut run la commande suivante depuis le répertoire glassfish5/bin

```java
asadmin set "server.java-config.java-home=path-to-java-home"
```

### client

* Se placer dans le dossier client
* pip install -r requirements.txt
* Dans le fichier main.py se trouve les différentes méthodes d'authentification, de recherche et de réservation. Commenter ou décommenter ce que vous souhaiter tester et lancer le fichier.**s**

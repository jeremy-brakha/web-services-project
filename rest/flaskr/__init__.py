import os

from flask import Flask, Blueprint, jsonify, request
from flaskr.db import get_db


bp = Blueprint('trains', __name__, url_prefix='/trains')


@bp.route('/', methods=['GET'])
def get_trains():
    '''Récupère les trains.'''
    db = get_db()
    trains = db.execute(
        'SELECT * FROM trains'
    ).fetchall()

    # Retourne les trains au format JSON
    # Affichage temporaire
    return jsonify([dict(train) for train in trains])


@bp.route('/<int:id>', methods=['GET'])
def get_train(id):
    '''Récupère un train en fonction de son id.'''
    db = get_db()
    train = db.execute(
        'SELECT * FROM trains WHERE id = ?', (id,)
    ).fetchone()

    if not train:
        return jsonify({"Erreur": "Aucun train trouvé."})

    return jsonify(dict(train))


@bp.route('/search_trains/<string:departure_station>/<string:arrival_station>/<string:outbound_date>/<string:return_date>/<int:number_of_tickets>/<string:travel_class>', methods=['GET'])
def search_trains(departure_station, arrival_station, outbound_date, return_date, number_of_tickets, travel_class):
    '''Recherche et renvoie les trains en fonction des paramètres de recherche.'''
    db = get_db()
    trains_one_way = db.execute(
        'SELECT * FROM trains WHERE departure_station = ? AND arrival_station = ? AND outbound_date = ? AND {} >= ?'.format(
            travel_class),
        (departure_station, arrival_station,
         outbound_date, number_of_tickets)
    ).fetchall()

    trains_other_way = db.execute(
        'SELECT * FROM trains WHERE departure_station = ? AND arrival_station = ? AND outbound_date = ? AND {} >= ?'.format(
            travel_class),
        (arrival_station, departure_station,
         return_date, number_of_tickets)
    ).fetchall()

    trains = trains_one_way + trains_other_way

    if not trains:
        return jsonify({"Erreur": "Aucun train disponible pour les critères de recherche."})

    return [dict(train) for train in trains]

@bp.route('/search_trains/departure_arrival_outbound_return/<string:departure_station>/<string:arrival_station>/<string:outbound_date>/<string:return_date>', methods=['GET'])
def search_trains_departure_station_arrival_station_outbound_date_arrival_date( departure_station, arrival_station, outbound_date, return_date):
    '''Recherche et renvoie les trains en fonction des paramètres de recherche.'''
    db = get_db()
    trains_one_way = db.execute(
        'SELECT * FROM trains WHERE departure_station = ? AND arrival_station = ? AND outbound_date = ?', (
            departure_station, arrival_station, outbound_date)
    ).fetchall()

    trains_other_way = db.execute(
        'SELECT * FROM trains WHERE departure_station = ? AND arrival_station = ? AND outbound_date = ?', (
            arrival_station, departure_station, return_date)
    ).fetchall()

    trains = trains_one_way + trains_other_way

    if not trains:
        return jsonify({"Erreur": "Aucun train disponible pour les critères de recherche."})

    return [dict(train) for train in trains]

@bp.route('/search_trains/arrival_station/<string:arrival_station>', methods=['GET'])
def search_trains_arrival_station(arrival_station):
    '''Recherche et renvoie les trains en fonction de la station d'arrivée.'''
    db = get_db()
    trains = db.execute(
        'SELECT * FROM trains WHERE arrival_station = ?',
        (arrival_station,)
    ).fetchall()

    if not trains:
        return jsonify({"Erreur": "Aucun train disponible pour cette station d'arrivée."})

    return [dict(train) for train in trains]


@bp.route('/search_trains/outbound_date/<string:outbound_date>', methods=['GET'])
def search_trains_outbound_date(outbound_date):
    '''Recherche et renvoie les trains en fonction de la date de départ.'''
    db = get_db()
    trains = db.execute(
        'SELECT * FROM trains WHERE outbound_date = ?',
        (outbound_date,)
    ).fetchall()

    if not trains:
        return jsonify({"Erreur": "Aucun train disponible pour cette date de départ."})

    return [dict(train) for train in trains]

@bp.route('/search_trains/travel_class/<string:travel_class>', methods=['GET'])
def search_trains_travel_class(travel_class):
    '''Recherche et renvoie les trains en fonction de la classe de voyage.'''
    db = get_db()
    trains = db.execute(
        'SELECT * FROM trains WHERE {} >= 1'.format(travel_class)
    ).fetchall()

    if not trains:
        return jsonify({"Erreur": "Aucun train disponible pour cette classe de voyage."})

    return [dict(train) for train in trains]


@bp.route('/search_trains/tickets_number/<string:travel_class>/<string:number_of_tickets>', methods=['GET'])
def search_trains_travel_class_number_of_tickets(travel_class, number_of_tickets):
    '''Recherche et renvoie les trains en fonction de la classe de voyage et du nombre de billets.'''
    db = get_db()
    trains = db.execute(
        'SELECT * FROM trains WHERE {} >= ?'.format(
            travel_class),
        (number_of_tickets,)
    ).fetchall()

    if not trains:
        return jsonify({"Erreur": "Aucun train disponible pour cette classe de voyage et ce nombre de billets."})

    return [dict(train) for train in trains]


@bp.route('/search_trains/tickets_number/<string:number_of_tickets>', methods=['GET'])
def search_trains_number_of_tickets(number_of_tickets):
    '''Recherche et renvoie les trains en fonction du nombre de billets.'''
    db = get_db()
    trains = db.execute(
        'SELECT * FROM trains WHERE standard_class_seats >= ? OR business_class_seats >= ? OR first_class_seats >= ?',
        (number_of_tickets, number_of_tickets, number_of_tickets)
    ).fetchall()

    if not trains:
        return jsonify({"Erreur": "Aucun train disponible pour ce nombre de billets."})

    return [dict(train) for train in trains]


@bp.route('/search_trains/<string:departure_station>/<string:arrival_station>', methods=['GET'])
def search_trains_departure_arrival_station(departure_station, arrival_station):
    '''Recherche et renvoie les trains en fonction de la station de départ et d'arrivée.'''
    db = get_db()
    trains_one_way = db.execute(
        'SELECT * FROM trains WHERE departure_station = ? AND arrival_station = ?',
        (departure_station, arrival_station)
    ).fetchall()

    trains_other_way = db.execute(
        'SELECT * FROM trains WHERE departure_station = ? AND arrival_station = ?',
        (arrival_station, departure_station)
    ).fetchall()

    trains = trains_one_way + trains_other_way

    if not trains:
        return jsonify({"Erreur": "Aucun train disponible pour ces stations."})

    return [dict(train) for train in trains]

@bp.route('/search_trains/<string:departure_station>/<string:arrival_station>/<string:outbound_date>', methods=['GET'])
def search_trains_departure_arrival_station_outbound_date(departure_station, arrival_station, outbound_date):
    '''Recherche et renvoie les trains en fonction de la station de départ, d'arrivée et de la date de départ.'''
    db = get_db()
    trains = db.execute(
        'SELECT * FROM trains WHERE departure_station = ? AND arrival_station = ? AND outbound_date = ?',
        (departure_station, arrival_station, outbound_date)
    ).fetchall()

    if not trains:
        return jsonify({"Erreur": "Aucun train disponible pour ces stations et cette date de départ."})

    return [dict(train) for train in trains]


@bp.route('/search_trains/<string:departure_station>/<string:arrival_station>/<string:outbound_date>/<string:return_date>', methods=['GET'])
def search_trains_departure_arrival_station_outbound_date_return_date(departure_station, arrival_station, outbound_date, return_date):
    '''Recherche et renvoie les trains en fonction de la station de départ, d'arrivée, de la date de départ et de la date de retour.'''
    db = get_db()
    trains_one_way = db.execute(
        'SELECT * FROM trains WHERE departure_station = ? AND arrival_station = ? AND outbound_date = ?', (
            departure_station, arrival_station, outbound_date)
    ).fetchall()

    trains_other_way = db.execute(
        'SELECT * FROM trains WHERE departure_station = ? AND arrival_station = ? AND outbound_date = ?', (
            arrival_station, departure_station, return_date)
    ).fetchall()

    trains = trains_one_way + trains_other_way

    if not trains:
        return jsonify({"Erreur": "Aucun train disponible pour ces stations, ces dates de départ et de retour."})

    return [dict(train) for train in trains]


@bp.route('/book', methods=['POST'])
def book_train():
    '''Réserve un train en fonction de l'ID du train, du type de classe et de la fléxibilité.'''
    train_id = request.form['train_id']
    travel_class = request.form['travel_class']

    db = get_db()

    train = db.execute(
        'SELECT * FROM trains WHERE id = ?',
        (train_id)
    ).fetchone()

    if not train:
        return jsonify({"Erreur": "Id de train."})
    else:
        db.execute(
            'UPDATE trains SET {} = {} - 1 WHERE id = ?'.format(
                travel_class, travel_class),
            (train_id,)
        )
        db.commit()
        return jsonify(dict(train))


def create_app(test_config=None):
    ''' Crée et configure l'application.'''
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    if test_config is None:
        # Charge l'instance de configuration, si elle existe, quand elle n'est pas passée en paramètre
        app.config.from_pyfile('config.py', silent=True)
    else:
        # Charge la configuration de test si elle est passée en paramètre
        app.config.from_mapping(test_config)

    # Assure que l'instance existe
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import db
    db.init_app(app)
    app.register_blueprint(bp)

    return app

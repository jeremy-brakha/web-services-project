from zeep_client import search_trains_departure_arrival_station_outbound_date,searchTrains, search_trains_departure_station_arrival_station_outbound_date_arrival_date, search_trains_departure_station_arrival_station, search_trains_travel_class_number_of_tickets, search_trains_travel_class, search_trains_outbound_date, authenticate_user, register, get_all_trains, search_train_arrival_station, book_train, search_trains_number_of_tickets

# registerUser = register("admin", "admin")
# print(registerUser)

# authenticateUser = authenticate_user("admin", "admin")
# print(authenticateUser)

# book = book_train(2, "standard_class_seats", True)
# print(book)



all_trains = get_all_trains()
# print(all_trains)

#Recherche de trains comportant 2 tickets
trains_by_number_of_tickets = search_trains_number_of_tickets(2)
# print(trains_by_number_of_tickets)

#Recherche de trains allant à Patsi 
trains_by_arrival_station = search_train_arrival_station("Patsi")
# print(trains_by_arrival_station)

#Recherche de trains partant le 20/09/2022
trains_by_outbound_date = search_trains_outbound_date("2022-09-20")
# print(trains_by_outbound_date)

#Recherche de trains ayant des billets standards
trains_by_travel_class = search_trains_travel_class("standard_class_seats")
# print(trains_by_travel_class)

#Recherche de billets standards dans des trains comportant 200 places disponibles
trains_by_travel_class_number_of_tickets = search_trains_travel_class_number_of_tickets("standard_class_seats", 200)
# print(trains_by_travel_class_number_of_tickets)

#Recherche de trains partant de Siruma et arrivant à Patsi
trains_by_departure_station_arrival_station = search_trains_departure_station_arrival_station("Siruma", "Patsi")
# print(trains_by_departure_station_arrival_station)

#Exemple de recherche de trains partant de Siruma le 31/10/2022 et arrivant à Patsi le 07/06/2024
trains_by_departure_station_arrival_station_outbound_date_arrival_date = search_trains_departure_station_arrival_station_outbound_date_arrival_date(
    "Siruma", "Patsi", "2022-10-31", "2024-06-07")
# print(trains_by_departure_station_arrival_station_outbound_date_arrival_date)


#Exemple de recherche de billets standards dans des trains partant de Siruma le 31/10/2022 et arrivant à Patsi le 07/06/2024 et comportant 3 places disponibles
trains = searchTrains("Siruma", "Patsi", "2022-10-31","2024-06-07", "standard_class_seats", 3)
# print(trains)


#Exemple de recherche de trains partant de Siruma le 31/10/2022 et arrivant à Patsi
trains_by_departure_arrival_station_outbound_date = search_trains_departure_arrival_station_outbound_date( "Siruma", "Patsi", "2022-10-31")
# print(trains_by_departure_arrival_station_outbound_date)

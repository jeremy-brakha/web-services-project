from config import WSDL_URL
from zeep import client

client = client.Client(WSDL_URL)


def authenticate_user(username: str, password: str) -> bool:
    """
    Authentification de l'utilisateur.
    """
    auth_status = client.service.authenticateUser(username, password)
    return auth_status


def register(username: str, password: str) -> bool:
    """
    Inscription de l'utilisateur.
    """
    registration_status = client.service.registerUser(
        username, password)
    return registration_status

def get_all_trains() -> list:
    """
    Récupère tous les trains.
    """
    trains = client.service.getTrains()
    return trains


def searchTrains(departure_station: str, arrival_station: str, outbound_date: str, arrival_date: str, travel_class: str, number_of_tickets: int) -> list:
    """
    Recherche un train par la gare de départ, la gare d'arrivée, la date de départ, la date d'arrivée, la classe de voyage, le nombre de billets disponibles et la flexibilité.
    """
    trains = client.service.searchTrains(departure_station, arrival_station, outbound_date, arrival_date, travel_class, number_of_tickets)
    return trains

def search_train_arrival_station(arrival_station: str) -> list:
    """
    Recherche un train par sa gare de départ.
    """
    trains = client.service.search_trains_arrival_station(arrival_station)
    return trains


def search_trains_number_of_tickets(number_of_tickets: int) -> list:
    """
    Recherche un train par le nombre de billets disponibles.
    """
    trains = client.service.search_trains_number_of_tickets(number_of_tickets)
    return trains

def search_trains_outbound_date(outbound_date: str) -> list:
    """
    Recherche un train par la date de départ.
    """
    trains = client.service.search_trains_outbound_date(outbound_date)
    return trains

def search_trains_travel_class(travel_class: str) -> list:
    """
    Recherche un train par la classe de voyage.
    """
    trains = client.service.search_trains_travel_class(travel_class)
    return trains


def search_trains_travel_class_number_of_tickets (travel_class: str, number_of_tickets: int) -> list:
    """
    Recherche un train par la classe de voyage et le nombre de billets disponibles.
    """
    trains = client.service.search_trains_travel_class_number_of_tickets(travel_class, number_of_tickets)
    return trains


def search_trains_departure_station_arrival_station (departure_station: str, arrival_station: str) -> list:
    """
    Recherche un train par la gare de départ et la gare d'arrivée.
    """
    trains = client.service.search_trains_departure_station_arrival_station(departure_station, arrival_station)
    return trains


def search_trains_departure_station_arrival_station_outbound_date_arrival_date (departure_station: str, arrival_station: str, outbound_date: str, arrival_date: str) -> list:
    """
    Recherche un train par la gare de départ, la gare d'arrivée, la date de départ et la date d'arrivée.
    """
    trains = client.service.search_trains_departure_station_arrival_station_outbound_date_arrival_date(departure_station, arrival_station, outbound_date, arrival_date)
    return trains


def search_trains_departure_arrival_station_outbound_date (departure_station: str, arrival_station: str, outbound_date: str) -> list:
    """
    Recherche un train par la gare de départ, la gare d'arrivée et la date de départ.
    """
    trains = client.service.search_trains_departure_arrival_station_outbound_date(departure_station, arrival_station, outbound_date)
    return trains

def book_train(trainId: int, travelClass: str, flexible: bool) -> bool:
    """
    Réserve un train pour un utilisateur.
    """
    booking_status = client.service.bookTrain(trainId, travelClass, flexible)
    return booking_status
